
from selenium import webdriver
from pages.home.home_page import homePage
from pages.home.login_page import loginPage
import unittest


class TestHome(unittest.TestCase):
          baseURL = "https://beta.changemakers.ch/"
          driver = webdriver.Chrome("./driver/chromedriver")
          driver.implicitly_wait(3)
          driver.maximize_window()
          lp = loginPage(driver)
          hp = homePage(driver)


          def test_settings(self):
              self.driver.get(self.baseURL)
              self.lp.login("Marijana@mindnow.io", "Mara1991")
              self.hp.goToSettings("Marijana", "Vukovic")

          def test_forgotPassword(self):
              self.driver.get(self.baseURL)
              self.lp.clickLoginLink()
              self.hp.forgotPassword("Marijana@mindnow.io")

              

# py.test -s -v tests/home/home_tests.py
