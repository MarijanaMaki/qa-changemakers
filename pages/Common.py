class Common:

    def has_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath)
            return True
        except Exception: return False

    def has_class_name(self, classname):                    #classname
        try:
            self.driver.find_element_by_class_name(classname)    #classname
            return True
        except Exception: return False

    def get_by_xpath(self, field):
        return self.driver.find_element_by_xpath(field)

    def get_by_class_name(self, field):
        return self.driver.find_element_by_class_name(field)

    def get_by_css_selector(self, field):
        return self.driver.find_elements_by_css_selector(field)

    def enter_text(self, field, enter_text):
        self.get_by_xpath(field).send_keys(enter_text)

    def click(self, x):
        if self.has_xpath(x) is True:
            self.get_by_xpath(x).click()

        elif self.has_class_name(x) is True:
            self.get_by_class_name(x).click()
