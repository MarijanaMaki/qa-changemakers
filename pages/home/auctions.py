import time
from selenium.webdriver.common.by import By
from pages.locators import sing_in_button, email_field, password_field, login_button, login_icon, settings_button, \
    input_name, input_lastname, save_settings_button, forgot_pass, reset_pass_button, bid_now_button, bid_input, \
    bid_now_secund_button, lower_bid, over_sale_bid
from base.selenium_driver import SeleniumDriver
from selenium.webdriver.support.select import Select

class auctionsPage(SeleniumDriver):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    def getBidNowButton(self):
        return self.driver.find_element(By.XPATH, bid_now_button)

    def clickOnBidButton(self):
        self.elementClick(bid_now_button, locatorType="xpath")

    def getbidInput(self):
        return self.driver.find_element(By.XPATH, bid_input)

    def enterBid(self,number):
        self.getbidInput().send_keys(number)

    def getConfirmBidButton(self):
        return self.driver.find_element(By.XPATH, bid_now_secund_button)

    def clickOnConfirmBidButton(self):
        self.elementClick(bid_now_secund_button, locatorType="xpath")

    def verifyLowerBid(self):
      result = self.isElementPresent(lower_bid, byType="xpath")
      return result

    #def verifyOverSaleBid(self):
     # result = self.isElementPresent(over_sale_bid, byType="xpath")
     # return result

    def bid(self, number):
        self.clickOnBidButton()
        self.enterBid(number)
        time.sleep(2)
        self.clickOnConfirmBidButton()


