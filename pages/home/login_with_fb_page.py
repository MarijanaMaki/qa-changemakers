from selenium.webdriver.common.by import By

from base.selenium_driver import SeleniumDriver


#****locators***

sing_in_button="menu__link.menu__button"
fb_button="//div[@class='cm-modal__facebook-icon']//img"
fb_email_input="//input[@id='email']"
fb_pass_input="//input[@id='pass']"
fb_login_button=" //button[@id='loginbutton']"
text = "//div[@class='_xjg']//span[@class='fwb'][contains(text(),'è-catch')]"   # here should be a changemakers text/change when they fix

#reCaptcha

class loginFB(SeleniumDriver):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    def get_sing_in_button(self):
        return self.driver.find_element(By.CLASS_NAME, sing_in_button)

    def get_fb_button(self):
        return self.driver.find_element(By.XPATH, fb_button)

    def get_fb_email_input(self):
        return self.driver.find_element(By.XPATH, fb_email_input)

    def get_fb_pass_input(self):
        return self.driver.find_element(By.XPATH, fb_pass_input)

    def get_fb_login_button(self):
        return self.driver.find_element(By.XPATH, fb_login_button)

    def click_on_sing_in_button(self):
        self.elementClick(sing_in_button, locatorType="class")

    def click_on_fb_button(self):
        self.elementClick(fb_button, locatorType="xpath")

    def enterEmail(self, email):
        self.get_fb_email_input().send_keys(email)

    def enterPass(self, password):
        self.get_fb_pass_input().send_keys(password)

    def click_on_log_in_button(self):
        self.elementClick(fb_login_button, locatorType="xpath")

    def login_with_fb(self,email,password):
        self.click_on_sing_in_button()
        self.click_on_fb_button()
        self.enterEmail(email)
        self.enterPass(password)
        self.click_on_log_in_button()

    def get_a_text(self):
        return self.driver.find_element(By.XPATH, text)

    def verifyText(self):
        result = self.isElementPresent(text, byType="xpath")
        return result
