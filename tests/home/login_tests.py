import time
import pytest
from selenium import webdriver
from pages.locators import driver
from pages.home.login_page import loginPage
import unittest


#Test scenario without framework! #Test treba da izgleda tako sto ces pozvati samo login sa loginPage klase!

# chrome_path = r'/usr/local/bin/chromedriver'
# driver = webdriver.Chrome(executable_path=chrome_path)
# driver = webdriver.Chrome("./driver/chromedriver")

class TestLogin(unittest.TestCase):
          baseURL = "https://beta.changemakers.ch/"
          driver = webdriver.Chrome("./driver/chromedriver")
          driver.implicitly_wait(3)
          driver.maximize_window()
          lp = loginPage(driver)

          @pytest.mark.run(order=2)
          def test(self):
            self.driver.get(self.baseURL)
            time.sleep(2)
            self.lp.login("Marijana@mindnow.io", "Mara1991")
            result = self.lp.verifyLoginSuccessful()
            assert result == True

          @pytest.mark.run(order=1)
          def test_invalidLogin(self):
           self.driver.get(self.baseURL)
           self.lp.login("Marijana@mindnow.io43", "Mara199144")
          # result = self.lp.verifyLoginFailed()
          # assert result == True
          # driver.quit()

        #********* Ovo je test bez framowok a********* step 1

        #loginLink = driver.find_element(By.CLASS_NAME, "menu__link.menu__button")
        #loginLink.click()

        #emailField = driver.find_element(By.XPATH, "//input[@placeholder='Email']")
        #emailField.send_keys(username)

        #passwordFiled = driver.find_element(By.XPATH, "//input[@placeholder='Passwort']")
        #passwordFiled.send_keys(password)

        #loginButton = driver.find_element(By.XPATH, login_button)
        #loginButton.click()

       # userIcon = driver.find_element(By.XPATH, " //a[@class='menu__link menu__link--sm-hidden menu__icon']//img")
       # if userIcon is not None:
           #     print("Login successful!")
        #else:
               # print("Login Failed")

#py.test -s -v tests/home/login_tests.py
