import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from base.selenium_driver import SeleniumDriver

# ***locators*****
about_button = "//a[contains(text(),'About')]"
cornercard_fb=" //div[@class='cm-container']//div[1]//div[2]//div[1]//a[1]"
cornercard_insta="//div[@class='cm-container']//div[1]//div[2]//div[1]//a[2]"
cornecard_website=" //div[@class='cm-container']//div[1]//div[2]//div[1]//a[3]"

fb_verify_icon="//a[@class='_64-f']//span[contains(text(),'Cornèrcard')]"



class AboutPage(SeleniumDriver):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    def get_about_button(self):
        return self.driver.find_element(By.XPATH, about_button)

    def click_on_about_button(self):
        self.elementClick(about_button, locatorType="xpath")

    def get_cornecard_fb(self):
        return self.driver.find_element(By.XPATH, cornercard_fb)

    def click_on_corencard_fb(self):
        self.elementClick(cornercard_fb, locatorType="xpath")

    def window_back(self):
        window_before = self.driver.window_handles[0]
        self.driver.switch_to.window(window_before)

    def get_cornercard_insta(self):
         return self.driver.find_element(By.XPATH, cornercard_insta)

    def click_on_cornecard_insta(self):
        self.elementClick(cornercard_insta, locatorType="xpath")

    def get_cornecard_website(self):
         return self.driver.find_element(By.XPATH, cornecard_website)

    def click_on_cornecard_website(self):
        self.elementClick(cornecard_website, locatorType="xpath")

    def verifyFB(self):
            result = self.isElementPresent(fb_verify_icon, byType="xpath")
            return result  #this doesn't work

    def open_social_fb(self):
        self.click_on_about_button()
        self.click_on_corencard_fb()
        self.verifyFB()
        self.window_back()
        time.sleep(2)
        self.click_on_cornecard_insta()
        self.window_back()
        time.sleep(2)
        self.click_on_cornecard_website()





