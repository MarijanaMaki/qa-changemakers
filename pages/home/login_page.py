from selenium.webdriver.common.by import By
from pages.locators import sing_in_button, email_field, password_field, login_button, login_icon
from base.selenium_driver import SeleniumDriver


class loginPage(SeleniumDriver):
    # definisemo konsturktora, tu stavljamo i driver instancu  step 3
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver
        #self.sing_in_button = "menu__link.menu__button"    #ali ovo nije dobra praksa, vec da budu lokatori u posebnom fajlu

    def getLoginLink(self):
        return self.driver.find_element(By.CLASS_NAME, sing_in_button)  #u tutorijalu stavlja self.sing_in_button

    def getEmailFiled(self):
        return self.driver.find_element(By.XPATH, email_field)         #ali to kod mene ne radi

    def getPasswordFiled(self):
        return self.driver.find_element(By.XPATH, password_field)

    def getLoginButton(self):
        return self.driver.find_element(By.XPATH, login_button)

    def clickLoginLink(self):
        # self.elementClick(self.sing_in_button, locatorType="class")  #meni ovo ne radi!!!!!
        self.elementClick(sing_in_button, locatorType="class")

    def enterEmail(self, email):
        self.getEmailFiled().send_keys(email)

    def enterPassword(self,password):
        self.getPasswordFiled().send_keys(password)

    def clickLoginButton(self):
        self.elementClick(login_button, locatorType="xpath")


    def login(self,email="",password=""):
         self.clickLoginLink()
         self.enterEmail(email)
         self.enterPassword(password)
         self.clickLoginButton()

    def verifyLoginSuccessful(self):
        result = self.isElementPresent(login_icon, byType="xpath")
        return result

    def verifyLoginFailed(self):
        result = self.isElementPresent("form-error__text", byType="name")
        return result

     #prvo je ovo bilo, sada pravimo iznad metode step 2
     #def login(self, username, password):
     #loginLink = self.driver.find_element(By.CLASS_NAME, "menu__link.menu__button")
     #loginLink.click()

     #emailField = self.driver.find_element(By.XPATH, "//input[@placeholder='Email']")
     #emailField.send_keys(username)

     #passwordFiled = self.driver.find_element(By.XPATH, "//input[@placeholder='Passwort']")
     #passwordFiled.send_keys(password)

     #loginButton = self.driver.find_element(By.XPATH, "//button[@class='cm-modal__submit']")
     #loginButton.click()
    def verifyTitle(self):
          if "Neki tekst" in self.getTitle():
              return True
          else:
              return False

# py.test -s -v tests/home/login_tests.py
