import time
from selenium.webdriver.common.by import By
from pages.locators import sing_in_button, email_field, password_field, login_button, login_icon, settings_button, \
    input_name, input_lastname, save_settings_button, forgot_pass, reset_pass_button
from base.selenium_driver import SeleniumDriver
from selenium.webdriver.support.select import Select

class homePage(SeleniumDriver):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    def getUserIcon(self):
        return self.driver.find_element(By.XPATH, login_icon)

    def getSettingsButton(self):
        return self.driver.find_element(By.XPATH, settings_button)

    def clickUserIcon(self):
        self.elementClick(login_icon, locatorType="xpath")

    def clickSettings(self):
        self.elementClick(settings_button, locatorType="xpath")

    def getNameField(self):
        return self.driver.find_element(By.XPATH, input_name)

    def getLastNameField(self):
        return self.driver.find_element(By.XPATH, input_lastname)

    def enterName(self, name):
        self.getNameField().send_keys(name)

    def enterLastName(self, lastname):
        self.getLastNameField().send_keys(lastname)

    def select(self):
        obj=Select(self.driver.find_element(By.NAME, "age"))
        obj.select_by_index(2)

    def getSaveSettingsButton(self):
        return self.driver.find_element(By.XPATH, save_settings_button)

    def clickOnSaveSettings(self):
        self.elementClick(save_settings_button,locatorType="xpath")

    def clearInputName(self):
        self.elementClear(input_name, locatorType="xpath")

    def clearInputLastName(self):
        self.elementClear(input_lastname, locatorType="xpath")


    #def verifySavedSettings(self):
       # result = self.isElementPresent(By.XPATH, "//form[@class='ng-pristine ng-valid ng-touched submitted']")
       # return result


    def goToSettings(self, name, lastname):
        self.clickUserIcon()
        self.clickSettings()
        time.sleep(2)
        self.clearInputName()
        time.sleep(2)
        self.enterName(name)
        self.clearInputLastName()
        time.sleep(2)
        self.enterLastName(lastname)
        self.select()
        self.clickOnSaveSettings()
       # self.verifySavedSettings()


    def getEmailFiled(self):  # duple metode(kako pozvati samo one sa loginp?)
        return self.driver.find_element(By.XPATH, email_field)

    def enterEmail(self, email):
        self.getEmailFiled().send_keys(email)

    def getForgotPassLink(self):
        return self.driver.find_element(By.XPATH, forgot_pass)

    def clickOnForgotPass(self):
        self.elementClick(forgot_pass, locatorType="xpath")

    def getResetPassButton(self):
        return self.driver.find_element(By.XPATH, reset_pass_button)

    def clickOnResetPassButton(self):
        self.elementClick(reset_pass_button, locatorType="xpath")

    def forgotPassword(self, email):  # kako da pristupim metodi sa druge stranice?
        self.clickOnForgotPass()
        self.enterEmail(email)
        time.sleep(2)
        self.clickOnResetPassButton()
